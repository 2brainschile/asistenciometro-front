'use strict';

angular.module('myApp.tabletview2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/tabletview2', {
    templateUrl: 'tablet_view_2/tablet_view_2.html',
    controller: 'TabletView2Ctrl'
  });
}])

.controller('TabletView2Ctrl', [function() {

}]);