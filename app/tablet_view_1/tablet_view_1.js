'use strict';

angular.module('myApp.tabletview1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/tabletview1', {
    templateUrl: 'tablet_view_1/tablet_view_1.html',
    controller: 'TabletView1Ctrl'
  });
}])

.controller('TabletView1Ctrl', [function() {

}]);