'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', function($scope) {
	//  http://dtfkadzxii.localtunnel.me/log/create?type=ENTRADA&worker=3&datetime=2017-06-06T12:01:00
});

